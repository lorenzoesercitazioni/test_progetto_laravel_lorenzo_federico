<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">

    <!-- mio stile -->
    <link rel="stylesheet" href="/css/style.css">

    <title>AWEEEE</title>
  </head>
  <body>
  <!-- <div class="col-12">
      <button type="button" class="btn btn-primary">
        <a href="{{route('homepage')}}">ciao</a>
      </button>
    </div> -->
  <div class="immagine paesaggio">
  <div class="container bg-danger height-100">
        <div class="row align-items-center">
            <div class="col-12">
                 <h1 class="text-center">Hello, world!</h1>
            </div>
            <div class="col-12 col-md-8 text-danger display-1">
                <h3>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Suscipit nemo tempora recusandae tenetur ut libero ullam asperiores totam non temporibus, explicabo veniam itaque atque aliquid corrupti officiis aut dignissimos ab!</h3>
            </div>
            <div class="col-12 col-md-4">
                <img class="img-fluid" src="/img/digital.jpg" alt="">
            </div>         
        </div>
    </div>
    </div>
    <div class="testo">
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque, aut non facilis possimus, esse tempora unde cum laborum voluptatem architecto nobis officiis, quis magni harum ipsam velit eos optio voluptatum!</p>
    </div>
    
    <div class="immagine natura">
    <div class="container bg-danger height-100">
        <div class="row align-items-center">
            <div class="col-12">
                 <h1 class="text-center">Hello, world!</h1>
            </div>
            <div class="col-12 col-md-8 text-danger display-1">
                <h3>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Suscipit nemo tempora recusandae tenetur ut libero ullam asperiores totam non temporibus, explicabo veniam itaque atque aliquid corrupti officiis aut dignissimos ab!</h3>
            </div>
            <div class="col-12 col-md-4">
                <img class="img-fluid" src="/img/digital.jpg" alt="">
            </div>         
        </div>
    </div>
    </div>
    <div class="col-12">
        <div class="row">
            <div class="col-12 col-md-3">
                <div class="card">
                    <img src="..." class="card-img-top" alt="...">
                    <div class="card-body">
                       <h5 class="card-title">{{$annoucement['title']}}</h5>
                       <p class="card-text">{{$annoucement['description']}}</p>
                       <p class="card-text">{{$annoucement['price']}}</p>
                       <a href="{{route('annunci')}}" class="btn btn-primary">TORNA ANNUNCI</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /immagine natura -->

    <!-- testo descrittivo -->
    <div class="testo">
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque, aut non facilis possimus, esse tempora unde cum laborum voluptatem architecto nobis officiis, quis magni harum ipsam velit eos optio voluptatum!</p>
    </div>
    <!-- /testo descrittivo -->

    <!-- immagine paesaggio -->
    <div class="immagine paesaggio">
      <h2>Web design</h2>
    </div>
    <!-- /immagine paesaggio -->

    <script src="/js/javascript.js"> </script>

    

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>


  </body>
</html>