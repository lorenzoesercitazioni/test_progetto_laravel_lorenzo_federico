<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
})->name('homepage');



Route::get('/annunci', function () {
    $annoucements =[
        ['title'=>'Titolo 1', 'description'=>'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Suscipit nemo tempora recusandae tenetur ut libero ullam asperiores totam non temporibus, explicabo veniam itaque atque aliquid corrupti officiis aut dignissimos ab!', 'img'=>'', 'price'=>'25,00'],
        ['title'=>'Titolo 2', 'description'=>'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Suscipit nemo tempora recusandae tenetur ut libero ullam asperiores totam non temporibus, explicabo veniam itaque atque aliquid corrupti officiis aut dignissimos ab!', 'img'=>'', 'price'=>'55,00'],
        ['title'=>'Titolo 3', 'description'=>'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Suscipit nemo tempora recusandae tenetur ut libero ullam asperiores totam non temporibus, explicabo veniam itaque atque aliquid corrupti officiis aut dignissimos ab!', 'img'=>'', 'price'=>'75,00'],
        ['title'=>'Titolo 4', 'description'=>'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Suscipit nemo tempora recusandae tenetur ut libero ullam asperiores totam non temporibus, explicabo veniam itaque atque aliquid corrupti officiis aut dignissimos ab!', 'img'=>'', 'price'=>'100,00'],
    ];

    return view('annunci' , ['annoucements'=>$annoucements]);
})->name('annunci');

Route::get('/annunci/dettaglio/{title}',function($title){
    // dd($title);
    $annoucements =[
        ['title'=>'Titolo 1', 'description'=>'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Suscipit nemo tempora recusandae tenetur ut libero ullam asperiores totam non temporibus, explicabo veniam itaque atque aliquid corrupti officiis aut dignissimos ab!', 'img'=>'', 'price'=>'25,00'],
        ['title'=>'Titolo 2', 'description'=>'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Suscipit nemo tempora recusandae tenetur ut libero ullam asperiores totam non temporibus, explicabo veniam itaque atque aliquid corrupti officiis aut dignissimos ab!', 'img'=>'', 'price'=>'55,00'],
        ['title'=>'Titolo 3', 'description'=>'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Suscipit nemo tempora recusandae tenetur ut libero ullam asperiores totam non temporibus, explicabo veniam itaque atque aliquid corrupti officiis aut dignissimos ab!', 'img'=>'', 'price'=>'75,00'],
        ['title'=>'Titolo 4', 'description'=>'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Suscipit nemo tempora recusandae tenetur ut libero ullam asperiores totam non temporibus, explicabo veniam itaque atque aliquid corrupti officiis aut dignissimos ab!', 'img'=>'', 'price'=>'100,00'],
    ];
    foreach ($annoucements as $annoucement){
        if ($annoucement['title']==$title) {
            // dd($annoucement);
            return view('dettaglio', ['annoucement'=>$annoucement]);
        }
    }   
})->name('annunci.dettaglio');





